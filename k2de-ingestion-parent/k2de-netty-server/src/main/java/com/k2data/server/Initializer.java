package com.k2data.server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;

public class Initializer extends ChannelInitializer<SocketChannel> {
    public static final Logger LOG = LoggerFactory.getLogger(Initializer.class);
    private Map<String, ChannelHandler> handlers;

    public Initializer(LinkedHashMap<String, ChannelHandler> handlers) {
        this.handlers = handlers;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        if(handlers != null && handlers.size() > 0) {
            for (String key : handlers.keySet()) {
                ChannelHandler handler = handlers.get(key);
                LOG.info("ChannelHandler:{}", handler.getClass().getName());
                pipeline.addLast(key, handler);
            }
        }
    }
}