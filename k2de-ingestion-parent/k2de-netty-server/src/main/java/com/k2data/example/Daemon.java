package com.k2data.example;

import com.k2data.common.Constants;
import com.k2data.server.Initializer;
import com.k2data.server.Server;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by lijianing on 17-11-6.
 */
public class Daemon {
    private static final Logger LOG = LoggerFactory.getLogger(Daemon.class);

    public static void main(String[] args) throws InterruptedException {
        System.setProperty(Constants.TOPIC_NAME,"test");
        System.setProperty(Constants.KAFKA_PRODUCER_NUM,"10");
        System.setProperty(Constants.KAFKA_SERVER_URL,"192.168.130.31:9092,192.168.130.30:9092");
        LinkedHashMap<String, ChannelHandler> handlers = new LinkedHashMap<>();
        byte[] dilimiter = "()".getBytes(CharsetUtil.UTF_8);
        byte[] dilimiter2 = ")".getBytes(CharsetUtil.UTF_8);
        ByteBuf[] byteBufs = new ByteBuf[2];
        byteBufs[0]= Unpooled.copiedBuffer(dilimiter);
        byteBufs[1]=Unpooled.copiedBuffer(dilimiter2);
        handlers.put("framer", new DelimiterBasedFrameDecoder(10000, byteBufs));
        handlers.put("decoder", new StringDecoder());
        handlers.put("encoder", new StringEncoder());
        handlers.put("handler", new StringHandler());
        Initializer initializer = new Initializer(handlers);
        List<ChannelHandler> handlerList = new LinkedList<>();
        handlerList.add(initializer);
        Server server = new Server(handlerList);
        server.start();
    }
}
