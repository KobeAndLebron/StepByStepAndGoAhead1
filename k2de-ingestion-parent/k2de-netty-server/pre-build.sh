#!/bin/bash
SCRIPT_DIR=$(dirname $(readlink -e $0))
. ${SCRIPT_DIR}/image

set -ex

export MAVEN_OPTS="-Xms128m -Xmx2048m -Xss128m -XX:MaxPermSize=1024M -XX:+CMSClassUnloadingEnabled"

# maven build
mvn -f $SCRIPT_DIR/../../pom.xml -pl k2de-ingestion-parent/k2de-netty-server clean package -am -Dskip.ut=true -Dskip.it=true

