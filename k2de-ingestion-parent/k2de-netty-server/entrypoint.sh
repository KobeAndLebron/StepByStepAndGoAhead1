#!/bin/bash
set -x
base_dir=$(dirname $(readlink -e $0))

java -cp ${base_dir}/k2de-netty-server-1.0-SNAPSHOT.jar com.k2data.server.Daemon

tail -f /dev/null