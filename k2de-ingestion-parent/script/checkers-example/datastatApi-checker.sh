#!/bin/bash

rc="$(curl --connect-timeout 3 -m 3 -o /dev/null -s -w %{http_code} http://$1:$2/datastat/health)"
echo "http_code: ${rc}"

if [ "${rc}" == "200" ]; then
   echo "tomcat server fine."
else
   echo "tomcat server bad."
   exit 1
fi

st=`date -d '1 days ago' +%Y-%m-%d`
et=`date +%Y-%m-%d`
h=`date +%H`
num=`echo "$h"|awk '{print $0+0}'`
if [[ $num -lt 7 ]];
then
 echo "$num -lt 7"
 st=`date -d '2 days ago' +%Y-%m-%d`
fi

echo "curl -s http://$1:$2/datastat/count?fieldGroupId=$3&startTime=$st&endTime=$et"
s=`curl -s http://"$1":"$2"/datastat/count'?'fieldGroupId="$3"'&'startTime="$st"'&'endTime="$et"`

echo $s > ./return.json

minTs=`grep -Po 'minTs[" :]+\K[^"]+' ./return.json`
code=`grep -Po 'code[" :]+\K[^"]+' ./return.json`

minTs=${minTs%%,*}
code=${code%%,*}
i="}" 
minTs=${minTs%%$i*}
code=${code%%$i*}
rm ./return.json
echo "min ts:"$minTs
echo "code: "$code
if [[ $code -eq 0 ]];
then
  if [[ $minTs != "null"  ]];
  then
     echo "exit 0"
     exit 0
  else
     echo "minTs is null exit 1"
     exit 1
  fi
else
  echo "code is 1 exit 1"
  exit 1
fi
