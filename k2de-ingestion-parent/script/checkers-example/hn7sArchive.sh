#!/bin/bash

var=`DOCKER_HOST=$1 docker logs --tail 1 goldwind_$2_1`
echo $var
logDate=${var%%.*}
logDate=${logDate/T/' '}
echo "log date: $logDate"
logTime=`date -d "$logDate" +%s`
echo "log time: $logTime"
echo "now date: `date`"
nowTime=`date +%s`
echo "now time: $nowTime"
((diff=($nowTime-$logTime+60)/60))
code=$?
echo "diff time: $diff, code: $code"
if [[ $code -eq 0 ]];
then
  if [[ $diff -lt $3 ]];
  then
    echo "exit 0"
    exit 0
  else
    echo "$diff > $3 exit 1"
    exit 1
  fi
else
  echo "code !=0 exit 1"
  exit 1
fi
