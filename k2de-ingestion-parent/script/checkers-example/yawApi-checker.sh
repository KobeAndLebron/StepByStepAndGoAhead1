#!/bin/bash

rc="$(curl --connect-timeout $1 -m $2 -o /dev/null -s -w %{http_code} http://$3:$4/yaw/health)"

echo "http_code: ${rc}"

if [ "${rc}" == "200" ]; then
   echo "request success"
   exit 0
else
   echo "request failed,please check tomcat server."
   exit 1
fi
