#!/bin/bash

# 将本地备份文件同步到hdfs上.
# 通过文件的md5码来校验文件是否在上传过程中出现破损, 打包备份上个月的数据到hdfs上 （一天一个zip包）.

###
# 使用方法: ./ archiveFC.sh hdfsIp 风场名称 年 月 日（1~n）
# ./archiveFC.sh 10.1.235.2 shitangshan 2017 09 01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30
# 上传过程中损坏的文件将会留在：k2data@kmx-4:/disk2/toolsW/zipTmp, 可进行重新上传
###

if [ $# -lt 4 ]; then
    echo "error:parameter error."
    echo "desc:<activeHost,wfName[dashantai|shitangshan],year,month,days[example:01,02,...]>"
    exit 1
fi

wfName=$2
year=$3
month=$4
days=$5
activeHost=$1

if [[ "dashantai" == $wfName ]];
then
  dir="/disk2/dashantai-workspace"
  echo $baseDir
elif [[ "shitangshan" == $wfName ]];
then
  dir="/disk1/shitangshan-workspace"
  echo $baseDir
else
  echo "wind farm name is error."
  exit 1
fi

arr=$(echo $days|tr "," "\n")

echo "hdfs dfs -mkdir hdfs://$activeHost:8020/k2data/dataplatform/processing/archivedata/gw-farm-control/$wfName/$year"
hdfs dfs -mkdir hdfs://$activeHost:8020/k2data/dataplatform/processing/archivedata/gw-farm-control/$wfName/$year

for day in $arr;
do
 echo "cd $dir/$year/$year-$month-$day"
 cd $dir/$year/$year-$month-$day

 echo "zip -r /disk2/toolsW/zipTmp/$wfName/$year-$month-$day".zip" ./*"
 zip -r /disk2/toolsW/zipTmp/$wfName/$year-$month-$day".zip" ./*

done

echo "hdfs dfs -put /disk2/toolsW/zipTmp/$wfName/*.zip hdfs://$activeHost:8020/k2data/dataplatform/processing/archivedata/gw-farm-control/$wfName/$year"
hdfs dfs -put /disk2/toolsW/zipTmp/$wfName/*.zip hdfs://$activeHost:8020/k2data/dataplatform/processing/archivedata/gw-farm-control/$wfName/$year

echo "java -DHDFS_HOST=10.1.235.1,10.1.235.2'  -classpath gw-archive-tools-1.0-SNAPSHOT-jar-with-dependencies.jar com.k2data.gwsolution.utils.FileIntegrityDetection /disk2/toolsW/zipTmp/$wfName /k2data/dataplatform/processing/archivedata/gw-farm-control/$wfName/$year true"
java -DHDFS_HOST=10.1.235.1,10.1.235.2  -classpath /disk2/toolsW/gw-archive-tools-1.0-SNAPSHOT-jar-with-dependencies.jar com.k2data.gwsolution.utils.FileIntegrityDetection /disk2/toolsW/zipTmp/$wfName /k2data/dataplatform/processing/archivedata/gw-farm-control/$wfName/$year true

