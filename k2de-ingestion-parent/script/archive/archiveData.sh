#!/bin/bash

# 对今天零点和昨天的数据进行归档(将金凤发过来共享目录下的zip文件拷贝到其他目录)
# 入参: 第一个参数: 要备份的工厂名字; 由此名字得到此工厂原始数据所做的目录(/disk2/share/${FACTORY_NAME}/)
#      第二个参数: 备份文件所要备份的目的目录.
# 例子: ./archiveData.sh dashantai /disk2/dashantai-workspace/

FILENAME_PREFIX="652211_"

today=$(date +%F)
yesterday=$(date -d"${today}-1day" +%F)
yearOfToday=$(date -d"${today}" +%Y)
yearOfYesterday=$(date -d"${yesterday}" +%Y)

filenameOfToday=${today//-/''}
filenameOfYesterday=${yesterday//-/''}

isBackedUp=0
# 验证是否数据已经备份过.
function verifyBackup(){
    directoryOfOriginFiles=$1
    dirOfArchive=$2
    filePattern=$3
    objToArchive=$4
    FACTORY_NAME=$5

    echo "Verify whether or not data of $objToArchive for $FACTORY_NAME has backed up?"

    if [ ! -d "$dirOfArchive" ]; then
        echo "Data of $objToArchive for $FACTORY_NAME has not backed up."
        isBackedUp=2
    else
        sumOfOriginalData=$(find ${directoryOfOriginFiles} -name "$filePattern" | wc -l) || exit 1
        sumOfArchiveData=$(find ${dirOfArchive} -name "$filePattern" | wc -l) || exit 1
        if [ ${sumOfArchiveData} -eq ${sumOfOriginalData} ]; then
            echo "Data of $objToArchive for $FACTORY_NAME has backed up."
            isBackedUp=0
        else
            echo "Data of $objToArchive for $FACTORY_NAME has not backed up."
            isBackedUp=1
        fi
    fi

}

# 先验证数据是否备份过, 如果备份过则结束; 否则进行备份.
function backUpDataByFactoryNameAndVerifyTheResult(){
    # 备份的工厂名
    FACTORY_NAME=$1

    directoryOfOriginFiles="/disk2/share/${FACTORY_NAME}/"
    directoryOfMoving=$2

    filePatternOfYesterday="${FILENAME_PREFIX}${filenameOfYesterday}*"
    dirOfArchiveForYesterday="${directoryOfMoving}/${yearOfYesterday}/${yesterday}/"


    verifyBackup "${directoryOfOriginFiles}" "${dirOfArchiveForYesterday}" "${filePatternOfYesterday}" \
                    "yesterday" "${FACTORY_NAME}"

    ## 备份昨天的数据
    if [ ${isBackedUp} -ne 0 ]; then
        echo "The process of archiving ${FACTORY_NAME} of yesterday starts..."

        mkdir -p ${dirOfArchiveForYesterday} || exit 1
        filesToArchive=$(find "${directoryOfOriginFiles}" -type f -name "$filePatternOfYesterday") || exit 1
        for fileToArchive in ${filesToArchive}; do
            cp -p ${fileToArchive} ${dirOfArchiveForYesterday} || exit 1
        done

        echo "The process of archiving ${FACTORY_NAME} of yesterday ends..."
    fi

    filePatternOfToday="${FILENAME_PREFIX}${filenameOfToday}00*"
    dirOfArchiveForToday="${directoryOfMoving}/${yearOfToday}/${today}/"
    verifyBackup "${directoryOfOriginFiles}" "${dirOfArchiveForToday}" "${filePatternOfToday}" \
                    "today" "${FACTORY_NAME}"

    ## 备份今天零点的数据
    if [ ${isBackedUp} -ne 0 ]; then
        echo "The process of archiving ${FACTORY_NAME} of today starts..."

        mkdir -p ${dirOfArchiveForToday} || exit 1
        filesToArchive=$(find "${directoryOfOriginFiles}"  -type f -name "$filePatternOfToday") || exit 1
        for fileToArchive in ${filesToArchive}; do
            cp -p ${fileToArchive} ${dirOfArchiveForToday} || exit 1
        done

        echo "The process of archiving ${FACTORY_NAME} of today ends..."
    fi

}


echo "Archive the files during $yesterday(yesterday) and $today 00H"

backUpDataByFactoryNameAndVerifyTheResult $1 $2
