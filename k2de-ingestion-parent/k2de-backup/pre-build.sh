#!/bin/bash
SCRIPT_DIR=$(dirname $(readlink -e $0))
set -ex

# maven build
mvn --also-make -f ${SCRIPT_DIR}/../../pom.xml -pl k2de-ingestion-parent/k2de-backup \
                                    clean package -am -Dskip.it=true -Dskip.ut=true

