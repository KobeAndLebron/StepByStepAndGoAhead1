# kafka-hdfs-sinker

This tool consumes data from kafka. Then output the messages onto HDFS.

Basic useful featurel list:
* user specify the kafka topic name;
* user determine the HDFS path by setting kafka message 'key';
* user can turn on compression ('bzip2' as splitable compression);
* user can set HDFS rotation size by message number;

## Examples
### jar
Build the jar as following:
```sh
cd /path/to/project/root
mvn clean assembly:assembly
```
Usage: read from kafka 'localhost' topic 'demoTopic', then output messages onto HDFS 'hdfs://localhost:9000' path '/demo/dir/subdir/subsubdir'.
```sh
export HDFS_URL='hdfs://localhost:9000' KAFKA_BROKERS='localhost' KAFKA_TOPIC='demoTopic' ZOOKEEPER_URL='localhost:2181' HDFS_PATH='/demo'; java -cp ./kafka-hdfs-sinker.jar consumer.ConsumerGroup dir_subdir_subsubdir
```
### docker image
Build the docker image (dev.k2data.com.cn:5001/k2data/kafka-hdfs-sinker:gw-1.2.1) as following:
```sh
cd /path/to/project/root
./pre-build.sh
./build.sh
```
Usage: read from kafka '10.1.10.21' topic 'demoTopic', then output messages onto HDFS 'hdfs://10.1.10.21:9000' path '/demo/dir1/subdir1/subsubdir1' and '/demo/dir2/subdir2/subsubdir2' based on kafka keys.
```sh
docker run -tid --name demo --add-host yn-ubuntu:10.1.10.21 -e HDFS_URL='hdfs://10.1.10.21:9000' -e KAFKA_BROKERS='10.1.10.21' -e KAFKA_TOPIC='demoTopic' -e ZOOKEEPER_URL='10.1.10.21:2181' -e HDFS_ROTATION_COUNT=1000 -e HDFS_PATH='/demo' -e KAFKA_KEYS='dir1_subdir1_subsubdir1,dir2_subdir2_subsubdir2' -e HADOOP_USER_NAME='hdfs' dev.k2data.com.cn:5001/k2data/kafka-hdfs-sinker:gw-1.2.1
```

## Configuration
| name   |  required  |      description      |  default  |  scope |
|----------|:-------------:|:-------------:|------:|------|
| ZOOKEEPER_URL| YES |   |  | jar/docker |
| KAFKA_BROKERS| YES | kafka broker ip list (no port). Comma separated. |  | jar/docker |
| HDFS_URL| YES |   |  | jar/docker |
| KAFKA_TOPIC| YES | topic name from which to read messages  |  | jar/docker |
| KAFKA_KEYS| YES | Accepted kafka keys. If set 'dir_subdir', only messages with key 'dir_subdir<KAFKA_KEY_RANDOM_SUFFIX>' can be consumed. Besides, the consumed messages will be store onto hdfs path '${HDFS_PATH}/dir/subdir'  |   | docker |
| HDFS_PATH| FALSE | basic path in hdfs to store the messages   | '/k2data/dataplatform/processing/abnormaldata/gw'  | jar/docker |
| HDFS_SYNC_COUNT| FALSE | hdfs sync every ${HDFS_SYNC_COUNT} messages  | 1000  | jar/docker |
| HDFS_ROTATION_COUNT| FALSE | hdfs rotate every ${HDFS_SYNC_COUNT} messages  | 100000  | jar/docker |
| HDFS_COMPRESS_CODEC| FALSE | hdfs compression method. 'nocomp'(no compression) or 'bzip2' | 'nocomp'  | jar/docker |
| KAFKA_BROKER_PORT| FALSE | kafka broker port | 9092  | jar/docker |
| KAFKA_KEY_RANDOM_SUFFIX_LEN| FALSE | random suffix length in kafka keys | 5  | jar/docker |

## How-to
### <a name="restart"></a>restart
1. Stop the java process: Ctrl + c
2. Find the last rotation offset from the stdout file;
3.  Reset the consumer offset in zookeeper: set /consumers/{topic}_{keytag}/offset/{topic}/{partition_num} last_rotation_offset
4. Start the service.

### recovery
The same as [restart](#restart) without step 1.

## Contact US
* Ning Yang: <yangning@k2data.com.cn>