package com.k2data.platform.backup.rotation;

/**
 * Created by yn on 3/16/17.
 */
public interface RotationPolicy extends Policy {
    /**
     * Do com.k2data.platform.backup.rotation or not
     *
     * @param message
     * @return
     */
    boolean mark(String message);

    /**
     * Reset the flag
     */
    void reset();
}
