package com.k2data.platform.backup.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InitConfigForBackup {
    private static final Logger LOGGER = LoggerFactory.getLogger(InitConfigForBackup.class);
    // wrap OutputStream with compression codec
    public static final String CODEC;
    public static final String HDFS_URL;
    public static final long HDFS_SYNC_COUNT;
    public static final long HDFS_ROTATION_COUNT;


    static {
        CODEC = ConfigUtil.getConfig(ParamNames.HDFS_COMPRESS_CODEC, Constants.HDFS_COMPRESS_CODEC);
        LOGGER.info("Code style:{}.", CODEC);

        HDFS_URL = ConfigUtil.getConfig(ParamNames.HDFS_URL);
        LOGGER.info("HDFS url:{}.", HDFS_URL);

        HDFS_SYNC_COUNT = Long.valueOf(ConfigUtil.getConfig(ParamNames.HDFS_SYNC_COUNT, "1000"));
        LOGGER.info("HDSF sync count:{}.", HDFS_SYNC_COUNT);

        HDFS_ROTATION_COUNT = Long.valueOf(ConfigUtil.getConfig(ParamNames.HDFS_ROTATION_COUNT, "100000"));
        LOGGER.info("HDFS rotation count:{}.", HDFS_ROTATION_COUNT);
    }
}
