package com.k2data.platform.backup.rotation;

public interface Policy {
    public long getThreshold();
}
