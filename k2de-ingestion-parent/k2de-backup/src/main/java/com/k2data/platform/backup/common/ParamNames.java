package com.k2data.platform.backup.common;

/**
 * Created by yn on 3/16/17.
 */
public class ParamNames {
    //Required config
    public final static String ZOOKEEPER_URL = "ZOOKEEPER_URL";
    public final static String KAFKA_BROKERS = "KAFKA_BROKERS";
    public final static String HDFS_URL = "HDFS_URL";
    public final static String KAFKA_TOPIC = "KAFKA_TOPIC";
    public final static String GROUP_NAME = "GROUP_NAME";

    //Optional config
    public final static String HDFS_PATH = "HDFS_PATH";
    public final static String HDFS_SYNC_COUNT = "HDFS_SYNC_COUNT";
    public final static String HDFS_ROTATION_COUNT = "HDFS_ROTATION_COUNT";
    public final static String HDFS_COMPRESS_CODEC = "HDFS_COMPRESS_CODEC";
    public final static String KAFKA_BROKER_PORT = "KAFKA_BROKER_PORT";
}
