package com.k2data.platform.backup.rotation;

/**
 * SyncPolicy implementation that will trigger a
 * file system sync after a certain number of tuples
 * have been processed.
 */
public class CountSyncPolicy implements SyncPolicy {
    private long threshold;
    private long count;

    public CountSyncPolicy(long threshold) {
        this.threshold = threshold;
        this.count = 0;
    }

    public boolean mark(String message) {
        this.count++;
        return this.count >= this.threshold;
    }

    public void reset() {
        this.count = 0;
    }

    @Override
    public long getThreshold() {
        return threshold;
    }
}
