package com.k2data.platform.backup.common;

/**
 * Created by yn on 3/16/17.
 */
public class ConfigUtil {
    /**
     * Get env config value.
     * @param name
     * @param defaultValue
     * @return
     */
    public static String getConfig(String name, String defaultValue) {
        String value = System.getenv(name);
        if (value == null) {
            return defaultValue;
        } else {
            return value;
        }
    }

    public static String getConfig(String name) {
        return getConfig(name, null);
    }
}
