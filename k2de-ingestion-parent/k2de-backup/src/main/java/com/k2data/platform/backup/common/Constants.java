package com.k2data.platform.backup.common;

import java.io.File;

/**
 * Created by yn on 3/16/17.
 */
public class Constants {
    public final static String HDFS_PATH = "/k2data/dataplatform/processing/archivedata/";
    public final static String HDFS_COMPRESS_CODEC = "nocomp";

    public final static int KAFKA_BROKER_PORT = 9092;
    public final static File STOP_SERVICE_FILE = new File("/opt/k2data/stopDir/stop");
}
