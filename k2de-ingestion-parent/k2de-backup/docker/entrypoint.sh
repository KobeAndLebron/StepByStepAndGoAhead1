#!/bin/bash
set -x
base_dir=$(dirname $(readlink -e $0))

if [[ -z "${KAFKA_KEYS}" ]]; then
  echo '${KAFKA_KEYS} is missing'
  exit -1
fi

IFS=',' read -ra exceptions <<< "${KAFKA_KEYS}"
for i in "${exceptions[@]}"; do
    echo "Processing exception [$i]"
    java -cp ${base_dir}/k2de-backup.jar com.k2data.platform.backup.consumer.KafkaConsumerToHDFS $i
done

tail -f /dev/null