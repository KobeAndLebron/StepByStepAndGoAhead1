#!/bin/bash
set -x
base_dir=$(dirname $(readlink -e $0))

if [[ -z "${CLASS_NAME}" ]]; then
  echo '${CLASS_NAME} is missing'
  exit -1
elif [ "${CLASS_NAME}" = 'Consume3sDataAndSend2KMX' ]; then
  echo "Processing exception ${CLASS_NAME}"
  java -cp ${base_dir}/k2de-kafka-consumer-1.0-SNAPSHOT.jar com.k2data.k2de.client.Consume3sDataAndSend2KMX
elif [ "${CLASS_NAME}" = 'ConsumeMessage' ]; then
  echo "Processing exception ${CLASS_NAME}"
  java -cp ${base_dir}/k2de-kafka-consumer-1.0-SNAPSHOT.jar com.k2data.k2de.client.ConsumeMessage
else
  echo 'CLASS_NAME can only be Consume3sDataAndSend2KMX or ConsumeMessage'
exit -1
fi

tail -f /dev/null