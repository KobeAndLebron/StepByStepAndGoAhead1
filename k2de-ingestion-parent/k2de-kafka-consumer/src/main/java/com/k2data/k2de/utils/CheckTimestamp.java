package com.k2data.k2de.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CheckTimestamp {
    private static final Logger LOGGER = LoggerFactory.getLogger(CheckTimestamp.class);

    /**
     * 校验时间格式,转化为符合kmx的时间格式 iso 或者 timestamp
     *
     * @param ts
     * @return
     */
    public static String validateAndProcessTime(String ts) {
        try {
            if (null == ts || "".equals(ts)) {
                LOGGER.warn("timestamp is null or empty !");
                return null;
            }
            //校验数据中ts的时间格式
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date tsDate = format.parse(ts);
            //获取当前时间 月份+1
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, 1);
            Date nextMonth = cal.getTime();
            //判断数据中的ts时间是否大于当前时间加1个月
            if (tsDate.getTime() > nextMonth.getTime()) { // similar to K2DB_REALTIME_REDIS_KEY_EXTENDED_TIME.
                LOGGER.warn("ts '" + ts + "' of data is greater than next month time.");
                return null;
            }
        } catch (ParseException e) {
            LOGGER.warn("The format of time string({}) is wrong:{}.", ts, e.getMessage());
            // 时间格式错误.
            return null;
        }
        //校验通过, 对数据进行时区处理
        ts = ts.replace(" ", "T");
        String iso = "yyyy-mm-ddTHH:MM:ss.sss";
        if (ts.length() <= iso.length()) {
            ts += "+08:00";
        }
        LOGGER.debug("Ts after processing:{}. ", ts);
        return ts;
    }

    /**
     * 校验时间格式,转化为符合kmx的时间格式 iso 或者 timestamp
     *
     * @param ts
     * @param isWithMills ts的精度是否到毫秒级别.
     * @return
     */
    public static String validateAndProcessTime(String ts, boolean isWithMills) {
        if (isWithMills) {
            return validateAndProcessTime(ts);
        } else {
            try {
                if (null == ts || "".equals(ts)) {
                    LOGGER.warn("timestamp is null or empty !");
                    return null;
                }
                //校验数据中ts的时间格式
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date tsDate = format.parse(ts);
                //获取当前时间 月份+1
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MONTH, 1);
                Date nextMonth = cal.getTime();
                //判断数据中的ts时间是否大于当前时间加1个月
                if (tsDate.getTime() > nextMonth.getTime()) { // similar to K2DB_REALTIME_REDIS_KEY_EXTENDED_TIME.
                    LOGGER.warn("ts '" + ts + "' of data is greater than next month time.");
                    return null;
                }
            } catch (ParseException e) {
                LOGGER.warn("The format of time string({}) is wrong:{}.", ts, e.getMessage());
                // 时间格式错误.
                return null;
            }
            //校验通过, 对数据进行时区处理
            ts = ts.replace(" ", "T");
            String iso = "yyyy-mm-ddTHH:MM:ss.sss";
            if (ts.length() <= iso.length()) {
                ts += "+08:00";
            }
            LOGGER.debug("Ts after processing:{}. ", ts);
            return ts;
        }
    }
}
