package com.k2data.k2de.utils;

import com.k2data.k2de.client.KafkaClient;
import com.k2data.platform.ddm.sdk.client.KMXClient;

import java.util.List;

public interface SendRecordToKMX {

    void sendRecord2KMXByMessage(String msg, KafkaClient clientForEx, KMXClient kmxClient, List<String> fieldGroupIds, String sdmApiUrl);
}
