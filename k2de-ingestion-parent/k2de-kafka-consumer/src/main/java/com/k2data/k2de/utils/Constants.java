package com.k2data.k2de.utils;

public class Constants {
    //k2-compose env
    public static String ZOOKEEPER_URL = "ZOOKEEPER_URL";
    public static String ZOOKEEPER_NAMESPACE = "ZOOKEEPER_NAMESPACE";

    public static String TOPIC_NAME = "TOPIC_NAME";
    public static String SDM_REST_API_URL = "SDM_REST_API_URL";
    //public static String FIELD_GROUP_ID = "FIELD_GROUP_ID";
    public static String BOOTSTRAP_SERVER = "BOOTSTRAP_SERVER";

}
