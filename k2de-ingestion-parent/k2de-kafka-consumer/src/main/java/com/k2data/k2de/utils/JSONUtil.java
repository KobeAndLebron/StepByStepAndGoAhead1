package com.k2data.k2de.utils;

import java.util.Map;

public interface JSONUtil {

    String jsonToString(String jsonStr);

    Map jsonToMap(String jsonStr);
}
