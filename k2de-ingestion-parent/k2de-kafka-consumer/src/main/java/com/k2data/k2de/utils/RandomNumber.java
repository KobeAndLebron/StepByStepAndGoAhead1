package com.k2data.k2de.utils;

import java.util.concurrent.ThreadLocalRandom;

public class RandomNumber {

    /**
     * 产生随机数
     *
     * @return
     */
    public static String getRandomNum() {
        return String.valueOf(ThreadLocalRandom.current().nextInt(10000, 100000));
    }
}
