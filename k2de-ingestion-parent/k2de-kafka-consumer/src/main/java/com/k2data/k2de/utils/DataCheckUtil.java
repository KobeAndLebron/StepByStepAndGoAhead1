package com.k2data.k2de.utils;

import com.k2data.k2de.client.KafkaClient;

import java.util.Map;

public interface DataCheckUtil {

    String[] checkData(String msg, KafkaClient clientForEx);

    Map<String, String> checkDataRMap(String msg, KafkaClient clientForEx);
}
