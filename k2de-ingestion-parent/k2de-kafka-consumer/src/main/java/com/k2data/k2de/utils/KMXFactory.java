package com.k2data.k2de.utils;

import com.k2data.platform.ddm.sdk.client.KMXClient;
import com.k2data.platform.ddm.sdk.client.KMXConfig;
import com.k2data.platform.ddm.sdk.common.DataType;

/**
 * create by chenming on 2017-12-1
 */
public class KMXFactory {

    private KMXFactory() {}

    /**
     * 创建 kmx client
     *
     * @param platformServer
     * @return
     */
    public static KMXClient createKmxClient(String platformServer) {
        KMXConfig config = new KMXConfig();
        config.put(com.k2data.platform.ddm.sdk.common.ParamNames.PLATFORM_SERVER, platformServer);
        config.put(com.k2data.platform.ddm.sdk.common.ParamNames.DATA_TYPE, DataType.BINARY);
        config.put(com.k2data.platform.ddm.sdk.common.ParamNames.SEND_TIMEOUT_MILLISECS, 60000);
        config.getConfig();
        config.put("acks", "1");
        return new KMXClient(config);
    }
}
