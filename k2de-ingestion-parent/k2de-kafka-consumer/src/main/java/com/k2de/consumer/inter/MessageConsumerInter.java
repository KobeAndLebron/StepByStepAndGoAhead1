package com.k2de.consumer.inter;

import com.k2de.consumer.result.KmxRecordResult;

/**
 * Created by chenjingshuai on 18-1-9.
 */
public interface MessageConsumerInter {
    /**
     * 消费指定topic的数据.
     * @param topic
     */
    void consume(String topic);

    /**
     * 设置将消息转化为KMX记录的转化器.
     *
     * @param converter
     */
    void setMessageToRecordConverter(MessageToRecordConverterInter converter);

    /**
     * 将消费来的Message转化后的KMXRecordResult发往指定的目的地.
     *
     * @param kmxRecordResult
     */
    void sendKmxRecordResult(KmxRecordResult kmxRecordResult);
}
