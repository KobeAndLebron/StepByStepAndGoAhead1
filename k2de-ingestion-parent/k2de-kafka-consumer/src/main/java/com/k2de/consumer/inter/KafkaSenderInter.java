package com.k2de.consumer.inter;

/**
 * Created by chenjingshuai on 18-1-9.
 */
public interface KafkaSenderInter<T> {

    void init(String kafkaServerUrl);

    void send(T sendOb);
}
