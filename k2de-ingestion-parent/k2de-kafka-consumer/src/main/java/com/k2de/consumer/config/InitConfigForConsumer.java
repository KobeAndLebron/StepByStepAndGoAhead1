package com.k2de.consumer.config;

import com.k2data.common.EnvConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chenjingshuai on 18-1-12.
 */
public class InitConfigForConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(InitConfigForConsumer.class);
    public static final String CONSUMER_GROUP_NAME = EnvConf.getEvnValue("CONSUMER_GROUP_NAME", "defaultGroup");
    public static final String OFFSET_WHEN_OFFSET_INFO_IS_ABSENT_IN_ZK =
            EnvConf.getEvnValue("OFFSET_WHEN_OFFSET_INFO_IS_ABSENT_IN_ZK", "smallest");


    static {
        LOGGER.info("Consumer group name:{}.", CONSUMER_GROUP_NAME);
        LOGGER.info("Offset when offset info is absent in zk:{}.", OFFSET_WHEN_OFFSET_INFO_IS_ABSENT_IN_ZK);
    }
}
