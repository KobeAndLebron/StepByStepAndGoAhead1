package com.k2de.consumer.impl;


import com.k2de.consumer.inter.MessageToRecordConverterInter;
import com.k2de.consumer.result.KmxRecordResult;

/**
 * 对Message不做任何处理.
 */
public class EmptyMessageToRecordConverterImpl implements MessageToRecordConverterInter {

    @Override
    public KmxRecordResult convertMessageToRecord(String message) {
        KmxRecordResult kmxRecordResult = new KmxRecordResult();
        kmxRecordResult.setOriginalMessage(message);
        return kmxRecordResult;
    }
}
