package com.k2de.consumer.inter;

import com.k2de.consumer.result.KmxRecordResult;

/**
 * 将Message转化为Record({@link KmxRecordResult})的转化器.
 *
 * Created by chenjingshuai on 18-1-9.
 */
public interface MessageToRecordConverterInter {

    /**
     * 转化Message为KmxRecordResult.
     *
     * @param message 将要转化的message.
     * @return 一定不为null.
     */
    KmxRecordResult convertMessageToRecord(String message);

}
