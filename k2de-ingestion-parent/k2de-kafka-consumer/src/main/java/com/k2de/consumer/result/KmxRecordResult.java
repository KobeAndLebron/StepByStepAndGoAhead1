package com.k2de.consumer.result;

import com.k2data.platform.ddm.sdk.builder.KMXRecord;

import java.util.Map;

/**
 * Created by chenjingshuai on 18-1-9.
 */
public class KmxRecordResult {
    /**
     * KmxRecord的转化是否成功; 成功则{@linkplain #kmxRecord}不为null;
     */
    private boolean isSuc;
    // 原始的message.
    private String originalMessage;
    // 转化后的结果.
    private KMXRecord kmxRecord;
    // kmxRecord代表的fieldGroup.
    private String fieldGroup;
    // kmxRecord代表的compoundId.
    private Map<String, String> compoundId;
    // compoundId的描述信息.
    private String compoundIdDescription;

    public KmxRecordResult() {
    }

    public KmxRecordResult(boolean isSuc, String originalMessage, KMXRecord kmxRecord, String fieldGroup, Map<String, String> compoundId) {
        this.isSuc = isSuc;
        this.originalMessage = originalMessage;
        this.kmxRecord = kmxRecord;
        this.fieldGroup = fieldGroup;
        this.compoundId = compoundId;
    }

    public boolean isSuc() {
        return isSuc;
    }

    public void setSuc(boolean suc) {
        isSuc = suc;
    }

    public String getOriginalMessage() {
        return originalMessage;
    }

    public void setOriginalMessage(String originalMessage) {
        this.originalMessage = originalMessage;
    }

    public KMXRecord getKmxRecord() {
        return kmxRecord;
    }

    public void setKmxRecord(KMXRecord kmxRecord) {
        this.kmxRecord = kmxRecord;
    }

    public Map<String, String> getCompoundId() {
        return compoundId;
    }

    public void setCompoundId(Map<String, String> compoundId) {
        this.compoundId = compoundId;
    }

    public String getFieldGroup() {
        return fieldGroup;
    }

    public void setFieldGroup(String fieldGroup) {
        this.fieldGroup = fieldGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KmxRecordResult that = (KmxRecordResult) o;

        if (isSuc != that.isSuc) return false;
        if (kmxRecord != null ? !kmxRecord.equals(that.kmxRecord) : that.kmxRecord != null) return false;
        if (fieldGroup != null ? !fieldGroup.equals(that.fieldGroup) : that.fieldGroup != null) return false;
        if (compoundId != null ? !compoundId.equals(that.compoundId) : that.compoundId != null) return false;
        return originalMessage != null ? originalMessage.equals(that.originalMessage) : that.originalMessage == null;

    }

    @Override
    public int hashCode() {
        int result = (isSuc ? 1 : 0);
        result = 31 * result + (kmxRecord != null ? kmxRecord.hashCode() : 0);
        result = 31 * result + (fieldGroup != null ? fieldGroup.hashCode() : 0);
        result = 31 * result + (compoundId != null ? compoundId.hashCode() : 0);
        result = 31 * result + (originalMessage != null ? originalMessage.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "KmxRecordResult{" +
                "isSuc=" + isSuc +
                ", kmxRecord=" + kmxRecord +
                ", fieldGroup='" + fieldGroup + '\'' +
                ", compoundId=" + compoundId +
                ", originalMessage='" + originalMessage + '\'' +
                '}';
    }

    public String getCompoundIdDescription() {
        return compoundIdDescription;
    }

    public void setCompoundIdDescription(String compoundIdDescription) {
        this.compoundIdDescription = compoundIdDescription;
    }
}
