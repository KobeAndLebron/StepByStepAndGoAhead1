package com.k2data.test;

import org.junit.Test;

public class Just4Test {

    @Test
    public void testJson() {

        String msg1 = "{\"date_time\":\"2017-11-30 01:00:00.0\",\"error\":0,\"feng\":0,\"feng_rate\":0,\"gu\":0,\"gu_rate\":0,\"" +
                "item_name\":\"4000_003_150300000004\",\"jian\":0,\"jian_rate\":0,\"ping\":7.7,\"ping_rate\":0,\"rate\":2.6,\"real_value\":0,\"value\":7.7}";
        String msg2 = "{\"Date_time\":\"2017-11-30 01:00:00.0\",\"Error\":0,\"Feng\":0,\"Feng_rate\":0,\"gu\":0,\"gu_rate\":0,\"" +
                "item_name\":\"4000_003_\",\"jian\":0,\"jian_rate\":0,\"ping\":7.7,\"ping_rate\":0,\"rate\":2.6,\"real_value\":0,\"value\":7.7}";
        String msg3 = "{\"Date_time\":\"2019-11-30 01:00:00.0\",\"Error\":0,\"Feng\":0,\"Feng_rate\":0,\"gu\":0,\"gu_rate\":0,\"" +
                "item_name\":\"4000_003_150300000004\",\"jian\":0,\"jian_rate\":0,\"ping\":7.7,\"ping_rate\":0,\"rate\":2.6,\"real_value\":0,\"value\":7.7}";
        String msg4 = "{\"Date_time\":\"2019-11-30 01:00:00.0\",\"Error\":0,\"Feng\":0,\"Feng_rate\":0,\"gu\":0,\"gu_rate\":0,\"" +
                "item_name\":\"4000_003_\",\"jian\":0,\"jian_rate\":0,\"ping\":7.7,\"ping_rate\":0,\"rate\":2.6,\"real_value\":0,\"value\":7.7}";
        String msg5 = "{\"Date_time\":\"2017-11-30 01:00:00.0\",\"Error\":0,\"Feng\":0,\"Feng_rate\":0,\"gu\":0,\"gu_rate\":0,\"" +
                "item_name\":\"\",\"jian\":0,\"jian_rate\":0,\"ping\":7.7,\"ping_rate\":0,\"rate\":2.6,\"real_value\":0,\"value\":7.7}";

        //System.out.println(new TCJSONUtil().jsonToString(msg2));
    }

    //异常信息条数
    public static Integer errMsgNum = 0;


    @Test
    public void testIntAndInteger() {

        IntegerTest integerTest = new IntegerTest();
        integerTest.IntPLus();
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~" + errMsgNum);

    }
}
