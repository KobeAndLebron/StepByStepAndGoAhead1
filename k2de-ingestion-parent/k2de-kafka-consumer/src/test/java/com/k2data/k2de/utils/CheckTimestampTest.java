package com.k2data.k2de.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by chenjingshuai on 18-1-16.
 */
public class CheckTimestampTest {

    @Test
    public void validateAndProcessTime() {
        String s = CheckTimestamp.validateAndProcessTime("2017-12-16 19:37:06", false);
        Assert.assertEquals(s, "2017-12-16T19:37:06+08:00");

        String s1 = CheckTimestamp.validateAndProcessTime("2017-12-16 19:37:06.000", true);
        Assert.assertEquals(s1, "2017-12-16T19:37:06.000+08:00");
    }
}