package com.k2data.common.tests.constants;

/**
 * 用户目录相关的常量(皆以'/'结尾).
 *
 * Created by ChenJingShuai on 2017/11/15.
 */
public class UserDirConstants {
    public static final String TEST_RESOURCES = System.getProperty("user.dir") + "/src/test/resources/";
    public static final String MAIN_RESOURCES = System.getProperty("user.dir") + "/src/main/resources/";
    public static final String OUTPUT_TEST_CLASSES = System.getProperty("user.dir") + "/target/test-classes/";
}
