package com.k2data.common.config;

public class PropertyConstants {
    public static final String POINT_TABLE_FILE_PATH = "POINT_TABLE_FILE_PATH";
    public static final String FIELDS_FILE_PATH = "FIELDS_FILE_PATH";
    public static final String DATATAG_2_DESCRI = "DATATAG_2_DESCRI";
    public static final String WHETHER_TO_FETCH_PROTOCOLFILE_WHEN_STARTED = "WHETHER_TO_FETCH_PROTOCOLFILE_WHEN_STARTED";
    public static final String PROTOCOLFILE_WGET_URL = "PROTOCOLFILE_WGET_URL";
    public static final String INTERVAL_SECONDS_OF_EXECUTING_UPDATE_PROTOCOLFILE = "INTERVAL_SECONDS_OF_EXECUTING_UPDATE_PROTOCOLFILE";
    public static final String APPENDING_DATA_TYPE_INFO = "APPENDING_DATA_TYPE_INFO";
    public static final String IMPORTED_SQL_FILE_PATH = "IMPORTED_SQL_FILE_PATH";
    public static final String WHETHER_TO_RECORD_METHOD_TIME = "WHETHER_TO_RECORD_METHOD_TIME";
}
