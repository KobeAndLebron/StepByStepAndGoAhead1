package com.k2data.common.config;

import com.k2data.common.EnvConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class InitConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(InitConfig.class);

    /**
     * 点表的绝对路径.
     * 值一般不用修改.
     */
    public static final String POINT_TABLE_FILE_PATH;

    /**
     *  格式： 各个参数件间用空格隔开, 参数内部用','隔开.
     *  单个参数的含义： 第一部分的值所代表的含义为原始数据中的数据种类标签（例如realtimedata tendata),
     * 第二部分的值所代表的含义为对应标签在idField（数据种类）下的值。
     *  比如 realtimedata,s 表示数据种类标签为‘realtimedata’的数据对应的模型的‘数据种类idField’的值为s。
     */
    public static final Map<String, String> DATATAG_2_DESCRI = new HashMap<>();

    /**
     * 存放fields文件的位置; Created in Dockerfile; NOTE:里面必须只有此类文件(可以包含目录), 不能有其他类型的文件, 否则程序会出错.
     * 值一般不用修改.
     */
    public static final String FIELDS_FILE_PATH;

    /**
     * 是否在容器启动的时候拉取协议点表文件.
     */
    public static final boolean WHETHER_TO_FETCH_PROTOCOLFILE_WHEN_STARTED;

    /**
     * 用wget获取协议电表文件的url地址.
     */
    public static final String PROTOCOLFILE_WGET_URL;

    /**
     * 在容器启动后每隔多长时间(单位:s)更新一次协议点表文件. 负值代表容器启动后不会进行协议点表的更新操作.
     * 默认频率为一天: 86400s.
     */
    public static final int INTERVAL_SECONDS_OF_EXECUTING_UPDATE_PROTOCOLFILE;

    /**
     *   代表含义： 添加的数据类型信息, 将会覆盖代码里定义的数据类型;
     *   格式：数据类型信息之间用空格隔开， 数据类型信息的field和数据类型用','隔开.
     *   e.g: "WTUR_TurSt_Rs_S,string WTUR_TurSt_Rs1_S,long"代表了WTUR_TurSt_Rs_S的数据类型将是string,
     *  WTUR_TurSt_Rs1_S的数据类型将是long.
     */
    public static final String APPENDING_DATA_TYPE_INFO;

    /**
     * 需要导入到点表库的数据库文件; Created in Dockerfile.
     */
    public static final String IMPORTED_SQL_FILE_PATH;

    /**
     * 是否记录方法的执行时间.
     */
    public static final boolean WHETHER_TO_RECORD_METHOD_TIME;

    static {
        POINT_TABLE_FILE_PATH = EnvConf.getEvnValue(
                PropertyConstants.POINT_TABLE_FILE_PATH, "/opt/k2data/protocols/configdata.file");
        LOGGER.info("Point table file path:{}.", POINT_TABLE_FILE_PATH);

        FIELDS_FILE_PATH = EnvConf.getEvnValue(PropertyConstants.FIELDS_FILE_PATH, "/opt/k2data/fields-files/");
        LOGGER.info("Fields file path:{}.", FIELDS_FILE_PATH);

        String dataTag2DescriParam = EnvConf.getEvnValue(
                PropertyConstants.DATATAG_2_DESCRI, "realtimedata,s tentimedata,tendata");
        String[] datatag2Descris = dataTag2DescriParam.split(" ");
        for (String datatag2Descri : datatag2Descris) {
            String[] split = datatag2Descri.split(",");
            DATATAG_2_DESCRI.put(split[0], split[1]);
        }
        LOGGER.info("Data tag to description map info: {}.", DATATAG_2_DESCRI);

        WHETHER_TO_FETCH_PROTOCOLFILE_WHEN_STARTED = Boolean.valueOf(
                EnvConf.getEvnValue(PropertyConstants.WHETHER_TO_FETCH_PROTOCOLFILE_WHEN_STARTED, "false"));
        LOGGER.info("Whether to fetch protocolFile when started:{}.", WHETHER_TO_FETCH_PROTOCOLFILE_WHEN_STARTED);

        PROTOCOLFILE_WGET_URL = EnvConf.getEvnValue(PropertyConstants.PROTOCOLFILE_WGET_URL, "");
        LOGGER.info("ProtocolFile 'wget' url:{}.", PROTOCOLFILE_WGET_URL);

        INTERVAL_SECONDS_OF_EXECUTING_UPDATE_PROTOCOLFILE = Integer.valueOf(EnvConf.getEvnValue(
                PropertyConstants.INTERVAL_SECONDS_OF_EXECUTING_UPDATE_PROTOCOLFILE, "86400"));
        LOGGER.info("Interval seconds of executing update protocol file:{}.", INTERVAL_SECONDS_OF_EXECUTING_UPDATE_PROTOCOLFILE);

        APPENDING_DATA_TYPE_INFO = EnvConf.getEvnValue(PropertyConstants.APPENDING_DATA_TYPE_INFO, "");
        LOGGER.info("Appending dataTypeInfo: {}.", APPENDING_DATA_TYPE_INFO);
        IMPORTED_SQL_FILE_PATH = EnvConf.getEvnValue(PropertyConstants.IMPORTED_SQL_FILE_PATH,
                "/opt/k2data/configdata_source_sql/configdata_k2.source.sql");
        LOGGER.info("Imported  sql file path:{}.", IMPORTED_SQL_FILE_PATH);

        WHETHER_TO_RECORD_METHOD_TIME = Boolean.valueOf(
                EnvConf.getEvnValue(PropertyConstants.WHETHER_TO_RECORD_METHOD_TIME, "false"));
        LOGGER.info("Whether to record method time? {}.", WHETHER_TO_RECORD_METHOD_TIME);
    }
}
