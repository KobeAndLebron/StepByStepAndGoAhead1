package com.k2data.common.entity;


import com.k2data.common.Constants;

import java.util.List;

/**
 * Created by ChenJingShuai on 2017/11/14.
 */
public class Field {
    private FieldGroup fieldGroup;
    private String id;
    private boolean isIdField;
    private String name;
    private String valueType;
    private List<Long> intervals;
    // optional.
    private String unit = Constants.EMPTY_CONTENT;
    private String description = Constants.EMPTY_CONTENT;

    public FieldGroup getFieldGroup() {
        return fieldGroup;
    }

    public void setFieldGroup(FieldGroup fieldGroup) {
        this.fieldGroup = fieldGroup;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isIdField() {
        return isIdField;
    }

    public List<Long> getIntervals() {
        return intervals;
    }

    public void setIntervals(List<Long> intervals) {
        this.intervals = intervals;
    }

    public void setIsIdField(boolean isIdField) {
        this.isIdField = isIdField;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }


    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Field field = (Field) o;

        if (isIdField != field.isIdField) return false;
        if (fieldGroup != null ? !fieldGroup.equals(field.fieldGroup) : field.fieldGroup != null) return false;
        if (id != null ? !id.equals(field.id) : field.id != null) return false;
        if (name != null ? !name.equals(field.name) : field.name != null) return false;
        if (valueType != null ? !valueType.equals(field.valueType) : field.valueType != null) return false;
        if (intervals != null ? !intervals.equals(field.intervals) : field.intervals != null) return false;
        if (unit != null ? !unit.equals(field.unit) : field.unit != null) return false;
        return description != null ? description.equals(field.description) : field.description == null;

    }

    @Override
    public int hashCode() {
        int result = fieldGroup != null ? fieldGroup.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (isIdField ? 1 : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (valueType != null ? valueType.hashCode() : 0);
        result = 31 * result + (intervals != null ? intervals.hashCode() : 0);
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Field{" +
                "fieldGroup=" + fieldGroup +
                ", id='" + id + '\'' +
                ", isIdField='" + isIdField + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", valueType='" + valueType + '\'' +
                ", intervals='" + intervals + '\'' +
                ", unit='" + unit + '\'' +
                '}';
    }
}
