package com.k2data.common.util;

/**
 * Created by chenjingshuai on 17-10-31.
 */
public class StringUtils {

    public static boolean isEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }
}
