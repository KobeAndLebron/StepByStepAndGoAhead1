package com.k2data.common;

import java.util.Map;

/**
 * Created by lijianing on 17-11-6.
 */
public class EnvConf {
    private static Map<String, String> env;

    private EnvConf() { }

    public static String getEvnValue(String key, String defaultValue) {
        if (null == env) {
            env = System.getenv();
        }
        String value = env.get(key);
        if (value == null || value.length() == 0) {
            value = System.getProperty(key);
        }
        if (value == null || value.length() == 0) {
            value = defaultValue;
        }
        return value;
    }
}
