package com.k2data.common;

/**
 * Created by lijianing on 17-11-6.
 */
public class Constants {
    // CSV文件分隔符.
    public static final String COL_SEPARATOR = ",";

    public static final String INTERVAL = "7000";
    // KMX平台Field各列的分隔符
    public static final String COLUMN_SEPARATOR = "_";

    public static final String EMPTY_CONTENT = "";

    // CSV文件分隔符.
    public static final String CSV_COL_SEPARATOR = ",";


    // KMX平台Field各列的分隔符
    public static final String KMX_COLUMN_SEPARATOR = "_";

    //kafka exception topic name
    public static String KAFKA_TOPIC_NAME_EXCEPTION = "defaultUser-defaultSpace-exception";

    public static final String CLIENT_HEART_BEAT_TEST_CONTENT = "comtest";
    public static final String RESPONSE_HEART_BEAT_TEST_SUCCESS = "1";
    public static final String RESPONSE_SUCCESS = "(ok)";
    public static final String RESPONSE_ERR = "(err)";
    //env
    public static final String KAFKA_PRODUCER_NUM = "KAFKA_PRODUCER_NUM";
    public static final String TOPIC_NAME = "TOPIC_NAME";
    public static final String SERVER_PORT_NUMBER = "SERVER_PORT_NUMBER";
    public static final String KAFKA_SERVER_URL = "KAFKA_SERVER_URL";

    //example
    public static String HN_7sDATA_KEY = "gw-scada-7s";

    // 3s   field value type
    public static final String FIELD_VALUE_TYPE = "DOUBLE";
}
