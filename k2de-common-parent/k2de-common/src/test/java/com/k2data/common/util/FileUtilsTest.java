package com.k2data.common.util;

import com.k2data.common.tests.constants.UserDirConstants;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by ChenJingShuai on 2017/11/17.
 */
public class FileUtilsTest {

    @Test
    public void getFileNameList() {
        List<String> filesByDirPath = FileUtils.getFileNameList(UserDirConstants.TEST_RESOURCES, true);
        System.out.println(filesByDirPath);
        Assert.assertEquals(3, filesByDirPath.size());
        Assert.assertEquals(true, filesByDirPath.get(0).contains(UserDirConstants.TEST_RESOURCES));

        filesByDirPath = FileUtils.getFileNameList(UserDirConstants.TEST_RESOURCES, false);
        Assert.assertEquals(3, filesByDirPath.size());
        System.out.println(filesByDirPath);
    }

}