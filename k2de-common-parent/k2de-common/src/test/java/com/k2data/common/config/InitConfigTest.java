package com.k2data.common.config;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * Created by ChenJingShuai on 2017/11/19.
 */
public class InitConfigTest {

    @Test
    public void TYPE_2_FIELDTest() {
        Map<String, String> datatag2Descri = InitConfig.DATATAG_2_DESCRI;
        Assert.assertEquals(2, datatag2Descri.size());
        Assert.assertEquals("s", datatag2Descri.get("realtimedata"));
    }

}