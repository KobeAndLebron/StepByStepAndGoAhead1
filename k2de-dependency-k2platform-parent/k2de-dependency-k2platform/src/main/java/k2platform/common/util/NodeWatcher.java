package k2platform.common.util;

import org.apache.curator.framework.recipes.cache.NodeCache;

import java.io.IOException;
import java.util.Map;

public abstract class NodeWatcher implements IWatcher {

    private NodeCache nodeCache;

    /**
     * sign load children data or node data, if true, load(byte[] data) will be called, else load(Map<String, byte[]> datas)
     **/
    private boolean nodeDataSign;

    public NodeWatcher() {
        nodeDataSign = true;
    }

    public NodeWatcher(boolean sign) {
        nodeDataSign = sign;
    }

    public boolean getNodeDataSign() {
        return this.nodeDataSign;
    }

    public void setNodeDataSign(boolean sign) {
        this.nodeDataSign = sign;
    }

    public void setNodeCache(NodeCache nodeCache) {
        this.nodeCache = nodeCache;
    }

    public void closeNodeCache() throws IOException {
        nodeCache.close();
    }

    /**
     * clear will be called before loading data from zk when node changes
     **/
    public abstract void clear();

    /**
     * Subclass should override one of load function at least
     **/
    public void load(byte[] data) {}

    ;

    public void load(Map<String, byte[]> datas) {}

    ;
}
