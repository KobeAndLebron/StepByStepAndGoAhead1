package k2platform.common.util;

public interface IService {

    void start();

    void close();
}
