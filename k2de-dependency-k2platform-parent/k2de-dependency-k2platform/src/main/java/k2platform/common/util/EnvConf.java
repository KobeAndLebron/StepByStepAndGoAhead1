package k2platform.common.util;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * Created by wangyn on 17-3-13.
 */
public class EnvConf {

    private static Map<String, String> env;

    private EnvConf() { }

    /**
     * 获取环境参数
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getEvnValue(String key, String defaultValue) {
        if (null == env) {
            env = System.getenv();
        }
        String value = env.get(key);
        if (StringUtils.isEmpty(value)) {
            value = defaultValue;
        }
        return value;
    }
}

