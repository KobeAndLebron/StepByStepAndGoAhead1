package k2platform.common.util;

public class ParamNames {

    public static final String CONF_FILE = "conf.file";

    //kafka
    public static final String METADATA_BROKER_LIST = "metadata.broker.list";
    public final static String KAFKA_TOPIC_NAME_ADAPTER = "kafka.topic.name.adapter";
    public final static String KAFKA_TOPIC_NAME_EXCEPTION = "kafka.topic.name.exception";
    public final static String KAFKA_TOPIC_NAME_AUDIT = "kafka.topic.name.audit";
    public final static String WRITE_PATH = "kafkaCount.txt";
    public static final String RAW_TOPIC_TOTAL = "rawTopicTotalRecordCounter";
    public static final String AVRO_TOPIC_TOTAL = "avroTopicTotalRecordCounter";
    public static final String EXCEPTION_TOPIC_TOTAL = "exceptionTopicTotalRecordCounter";

    //Zookeeper configuration names
    public static final String ZOOKEEPER_URL = "zookeeper.url";
    public static final String ZOOKEEPER_NAMESPACE = "zookeeper.namespace";

    //data field separator used in processing step
    public static final String PROCESSING_DATA_FIELD_SEPARATOR = "processing.data.field.separator";

    //data separator used in processing step
    public static final String PROCESSING_DATA_RECORD_SEPARATOR = "processing.data.record.separator";
    public static final String PROCESSING_RAWDATA_HDFS_DIR = "processing.rawdata.hdfs.dir";
    public static final String PROCESSING_CSVDATA_HDFS_DIR = "processing.csvdata.hdfs.dir";
    public static final String PROCESSING_ABNORMALDATA_HDFS_DIR = "processing.abnormaldata.hdfs.dir";
    public static final String PROCESSING_STREAM_FLAGS_HDFS_DIR = "processing.stream.flags.hdfs.dir";
    public static final String PROCESSING_BATCH_FLAGS_HDFS_DIR = "processing.batch.flags.hdfs.dir";
    public static final String PROCESSING_BACKUP_HDFS_DIR = "processing.backup.hdfs.dir";
    public static final String PROCESSING_METADATA_HDFS_DIR = "processing.metadata.hdfs.dir";
    public static final String PROCESSING_TIMESTAMP = "processing.timestamp";
    public static final String PROCESSING_HDFS_FILE_ROTATION = "processing.hdfs.file.rotation.min";
    public static final String PROCESSING_FILE_FILTER_SIZE = "processing.file.filter.size";
    public static final String PROCESSING_MULTI_THREAD = "processing.multi.thread";
    public static final String PROCESSING_THREAD_SIZE = "processing.thread.size";
    public static final String PROCESSING_SUBJOB_RETRY_TIME = "processing.subjob.retry.time";
    public static final String PROCESSING_DEVICE_METADATA_FILE_NAME = "processing.device.metadata.file.name";
    public static final String PROCESSING_SCHEMA_METADATA_FILE_NAME = "processing.schema.metadata.file.name";
    public static final String PROCESSING_PARTITION_METADATA_FILE_NAME = "processing.partition.metadata.file.name";

    public static final String PROCESSING_FLAGS_DATABASE = "processing.flags.database.name";
    public static final String PROCESSING_FLAGS_BATCH_TABLE = "processing.flags.batch.table.name";
    public static final String PROCESSING_FLAGS_STREAM_TABLE = "processing.flags.stream.table.name";
    public static final String PROCESSING_FLAGS_K2DB_TABLE = "processing.flags.k2db.table.name";
    public static final String PROCESSING_FLAGS_MYSQL_URL_BASE = "flags.mysql.url.base";
    public static final String PROCESSING_FLAGS_MYSQL_DRIVER_NAME = "flags.mysql.driver.name";
    public static final String PROCESSING_LOG_DATABASE = "processing.log.database.name";
    public static final String PROCESSING_LOG_K2DB_TABLE = "processing.log.k2db.table.name";
    public static final String PROCESSING_MYSQL_USER = "processing.mysql.user";
    public static final String PROCESSING_MYSQL_PASS = "processing.mysql.pass";


    //hadoop master configuration, from this configuration program will get all configuration remotely. e.g. 192.168.130.21:8080
    public static final String HADOOP_MASTER_URL = "hadoop.master.url";
    public static final String HADOOP_USER_NAME = "hadoop.user.name";

    //storm configuration
    public static final String NIMBUS_HOST = "nimbus.host";
    public static final String STORM_PARALLELISM = "storm.parallelism";
    public static final String STORM_DEBUG = "storm.debug";

    //impala configuration
    public static final String IMPALA_HOST = "impala.host";
    public static final String IMPALA_PORT = "impala.port";
    public static final String IMPALA_SCHEMA = "impala.schema";
    public static final String IMPALA_SOFT_DELETE = "impala.soft.delete";
    public static final String IMPALA_CONN_RETRY_TIME = "impala.conn.retry.time";
    public static final String IMPALA_CONN_GET_TIMEOUT = "impala.conn.get.timeout";

    //storage schedule time
    public static final String STORAGE_SCHEDULE_TIME = "storage.schedule.time";
    public static final String STORAGE_MR_DELETE_INPUT = "storage.mr.delete.input";
    public static final String DATA_TRANSFORM_REDUCE_NUMBER = "data.transform.reduce.number";
    public static final String DATA_TRANSFORM_REDUCE_MIN_NUMBER = "data.transform.reduce.min.number";

    //impala parq load
    public static final String IMPALA_PARQ_PARTITION_TIMEOUT = "parquet.load.timeout";
    public static final String IMPALA_PARQ_PARTITION_CHECK_INTERVAL = "parquet.load.check.interval";

    //topology parallelism configuration
    public static final String PROTOCOL_TOPOLOGY_WORKER = "protocol.topology.worker";
    public static final String PROTOCOL_TOPOLOGY_SPOUT_PARALLELISM = "protocol.topology.spout.parallelism";
    public static final String PROTOCOL_TOPOLOGY_TRANSFORM_BOLT_PARALLELISM = "protocol.topology.transform.bolt.parallelism";
    public static final String PROTOCOL_TOPOLOGY_KAFKA_BOLT_PARALLELISM = "protocol.topology.kafka.bolt.parallelism";
    public static final String MONITOR_TOPOLOGY_WORKER = "monitor.topology.worker";
    public static final String MONITOR_TOPOLOGY_SPOUT_PARALLELISM = "monitor.topology.spout.parallelism";
    public static final String MONITOR_TOPOLOGY_BOLT_PARALLELISM = "monitor.topology.bolt.parallelism";
    public static final String ADAPTER_TOPOLOGY_WORKER = "adapter.topology.worker";
    public static final String ADAPTER_TOPOLOGY_SPOUT_PARALLELISM = "adapter.topology.spout.parallelism";
    public static final String ADAPTER_TOPOLOGY_TRANSFORM_BOLT_PARALLELISM = "adapter.topology.transform.bolt.parallelism";
    public static final String ADAPTER_TOPOLOGY_KAFKA_BOLT_PARALLELISM = "adapter.topology.kafka.bolt.parallelism";
    public static final String HDFS_TOPOLOGY_WORKER = "hdfs.topology.worker";
    public static final String HDFS_TOPOLOGY_SPOUT_PARALLELISM = "hdfs.topology.spout.parallelism";
    public static final String HDFS_TOPOLOGY_BOLT_PARALLELISM = "hdfs.topology.bolt.parallelism";
    public static final String AUDIT_TOPOLOGY_WORKER = "audit.topology.worker";
    public static final String AUDIT_TOPOLOGY_SPOUT_PARALLELISM = "audit.topology.spout.parallelism";
    public static final String AUDIT_TOPOLOGY_BOLT_PARALLELISM = "audit.topology.bolt.parallelism";

    // audit
    public final static String AUDIT_TURN_ON = "audit.turn.on";
    public final static String AUDIT_INTERVAL_SECONDS = "audit.interval.seconds";
    public final static String AUDIT_MYSQL_URL = "audit.mysql.url";
    public final static String AUDIT_MYSQL_USER = "audit.mysql.user";
    public final static String AUDIT_MYSQL_PASS = "audit.mysql.pass";

    // netty client constants
    public final static String DISTINGUISH_MESSAGES = "distinguish.messages";

    //message queue broker url
    public final static String MQ_SERVER_URL = "mq.server.url";
    public final static String SCHEMA_QUEUE_SUFFIX = "schema.queue.suffix";

    //message handler thread pool size
    public final static String MESSAGE_HANDLER_THREAD_SIZE = "message.handler.thread.size";

    //configuration for mock-data
    public final static String REST_DATA_SERVICE_URL = "rest.data.service.url";
    public final static String BATCH_LOAD_CONF_FILE = "batch.load.conf.file";

    //sdm configuration
    public static final String SDM_REST_API_SCHEMA_PATH = "sdm.rest.api.schema.path";
    public static final String SDM_REST_API_DEVICE_PATH = "sdm.rest.api.device.path";

    public static final String DEBUG_MODE = "debug.mode";
}
