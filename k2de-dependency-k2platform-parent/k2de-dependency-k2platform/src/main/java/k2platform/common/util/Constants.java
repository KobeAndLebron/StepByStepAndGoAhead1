package k2platform.common.util;

public class Constants {

    public static final String KMX_TIME_ZONE = "Asia/Shanghai";
    public static final String KMX_MODE = "DOCKER";  //DOCKER or CDH

    //Cloudera Manager info
    public static final String CM_CLUSTER_NAME = "cluster";
    public static final String CM_REPLACE_CLUSTER_NAME = "{CLUSTER_NAME}";
    public static final String CM_REST_HDFS = "/api/v11/clusters/" + CM_REPLACE_CLUSTER_NAME + "/services/hdfs/clientConfig";
    public static final String CM_REST_YARN = "/api/v11/clusters/" + CM_REPLACE_CLUSTER_NAME + "/services/yarn/clientConfig";
    public static final String CM_REST_HBASE = "/api/v11/clusters/" + CM_REPLACE_CLUSTER_NAME + "/services/hbase/clientConfig";
    public static final String CM_SERVER_USER = "admin";
    public static final String CM_SERVER_PASSWD = "admin";

    //Configuration Constants
    public static final String CONF_COMPONENT_SYSTEM = "SYSTEM";
    public static final String CONF_COMPONENT_COMMON = "COMMON";
    public static final String CONF_COMPONENT_KAFKA = "KAFKA";
    public static final String CONF_COMPONENT_ADAPTER = "ADAPTER";
    public static final String CONF_COMPONENT_STORM = "STORM";
    public static final String CONF_COMPONENT_HADOOP = "HADOOP";
    public static final String CONF_COMPONENT_IMPALA = "IMPALA";

    public static final String SCHEMA_DT_PREFIX = "dt_";
    public static final String SCHEMA_ST_PREFIX = "st_";
    public static final String SCHEMA_ASSET_PREFIX = "a_";
    public static final String SCHEMA_DEVICE_ID = "deviceid";
    public static final String SCHEMA_TIMESTAMP = "timestamp";
    public static final String SCHEMA_FGSYSID = "fieldgroupsysid";
    public static final String SCHEMA_IDFIELD_ISKEY = "iskey";
    public static final String TIMESTAMP_TABLE_COLUMN_NAME = "ts";

    public static final String CONF_DEFAULT_ENCODING = "UTF-8";

    //Zookeeper Constants
    public static final String ZOOKEEPER_NAMESPACE = "k2data";
    public static final String ZOOKEEPER_PLATFORM = "dataplatform";
    public static final String ZOOKEEPER_SCHEMA_PATH = "/" + ZOOKEEPER_PLATFORM + "/schema";
    public static final String ZOOKEEPER_PATH_RELOAD_NODE = "reload";
    public static final String ZOOKEEPER_SCHEMA_RELOAD_PATH = ZOOKEEPER_SCHEMA_PATH + "/" + ZOOKEEPER_PATH_RELOAD_NODE;
    public static final String ZOOKEEPER_ASSET_PATH = "/" + ZOOKEEPER_PLATFORM + "/asset";
    public static final String ZOOKEEPER_CONF_PATH = "/" + ZOOKEEPER_PLATFORM + "/configuration";
    public static final String ZOOKEEPER_STORM_PATH = "/" + ZOOKEEPER_PLATFORM + "/storm";
    public static final String ZOOKEEPER_FILE_PATH = "src/test/resources/zookeeper.properties";
    public static final String ZOOKEEPER_LOCK_PATH = "/" + ZOOKEEPER_PLATFORM + "/lock";
    public static final String ZOOKEEPER_WORKFLOW_PATH = "/" + ZOOKEEPER_PLATFORM + "/workflow";

    public static final int ZOOKEEPER_RETRY_TIME = Integer.MAX_VALUE;
    public static final int ZOOKEEPER_SLEEP_BETWEEN_RETRY = 1000;
    public static final int ZOOKEEPER_CONNECTION_TIMEOUT = 5000;
    public static final int ZOOKEEPER_SESSION_TIMEOUT = 5000;

    //Storm constants
    public static final int STORM_PARALLELISM = 1;
    public static final String STORAGE_SCHEDULE_TIME = "1";
    public static final String DID_REGISTER_SCHEDULE_TIME = "1";
    public static final boolean STORM_DEBUG = false;

    //System Constants
    public static final String UNIX_FILE_SEPARATOR = "/";

    // Kafka Constants
    public final static String KAFKA_TOPIC_NAME_ADAPTER = "sample";
    public final static String KAFKA_TOPIC_NAME_ADAPTER_BINARY = "binary";
    public final static String KAFKA_TOPIC_NAME_MONITOR = "monitor";
    public final static String KAFKA_TOPIC_NAME_EXCEPTION = "exception";
    public final static String KAFKA_TOPIC_NAME_AUDIT = "audit";
    public final static String KAFKA_DEFAULT_FIELD_KEY = "key";
    public final static String KAFKA_DEFAULT_FIELD_MESSAGE = "message";
    public final static int KAFKA_TOPIC_REPLICATION_FACTOR = 2;
    public final static String KAFKA_REQUEST_REQUIRED_ACKS = "all";  //0: no ack; 1: only leader ack; all: leader and all followers ack.
    public final static int KAFKA_RETRIES = 3;  //Setting a value greater than zero will cause the client to resend any record whose send fails with a potentially transient error.
    //batch loader
    public final static int BATCH_LOAD_CHECKER_INTERVAL_TIME_SECONDS = 30;
    public final static int BATCH_LOAD_CLEANER_INTERVAL_TIME_SECONDS = 60;
    public final static int BATCH_LOAD_SCHEDULER_INTERVAL_TIME_MILLS = 10000;
    public final static boolean BATCH_LOAD_DO_AGGREGATION = true;
    //dataplatform processing constants
    public static final String PROCESSING_RAWDATA_HDFS_DIR = "/k2data/dataplatform/processing/rawdata";
    public static final String PROCESSING_CSVDATA_HDFS_DIR = "/k2data/dataplatform/processing/csvdata";
    public static final String PROCESSING_ABNORMALDATA_HDFS_DIR = "/k2data/dataplatform/processing/abnormaldata";
    public static final String PROCESSING_DIDEXDATA_HDFS_DIR = "/k2data/dataplatform/processing/didexdata";
    public static final String PROCESSING_STREAM_FLAGS_HDFS_DIR = "/k2data/dataplatform/processing/flags/stream";
    public static final String PROCESSING_BATCH_FLAGS_HDFS_DIR = "/k2data/dataplatform/processing/flags/batch";
    public static final String PROCESSING_BACKUP_HDFS_DIR = "/k2data/dataplatform/processing/backup";
    public static final String PROCESSING_METADATA_HDFS_DIR = "/k2data/dataplatform/processing/metadata";
    public static final String PROCESSING_BATCH_METADATA_HDFS_DIR = "/batch";
    public static final String PROCESSING_STREAM_METADATA_HDFS_DIR = "/stream";
    public static final float PROCESSING_HDFS_FILE_ROTATION = 20f;
    public static final float PROCESSING_DIDEX_HDFS_FILE_ROTATION = 3f;
    public static final String PROCESSING_ASSET_SCHEMA_FILE_NAME = "assetSchemaMap";
    public static final String PROCESSING_SCHEMA_METADATA_FILE_NAME = "/tmp/schema";
    public static final String PROCESSING_ASSET_METADATA_FILE_NAME = "/tmp/asset";
    public static final String PROCESSING_PARTITION_METADATA_FILE_NAME = "/tmp/partition";
    public static final String PROCESSING_SCHEMA_METADATA_CACHE_FILE_NAME = "schema";
    public static final String PROCESSING_ASSET_METADATA_CACHE_FILE_NAME = "asset";
    public static final String PROCESSING_PARTITION_METADATA_CACHE_FILE_NAME = "partition";
    public static final String PROCESSING_METADATA_COLUMN_SEPARATOR = "!@";
    public static final long PROCESSING_FILE_FILTER_SIZE = 130l;
    public static final boolean PROCESSING_MULTI_THREAD = true;
    public static final int PROCESSING_THREAD_SIZE = 10;
    public static final int PROCESSING_THREAD_TIMEOUT = 1; //hour
    public static final String PROCESSING_MR_CONF_PREFIX = "mr.";
    public static final int PROCESSING_SUBJOB_RETRY_TIME = 3;
    public static final int PROCESSING_WORKFLOW_RETRY_TIME = 2;
    public static final int PROCESSING_MIN_YEAR = 1970;
    public static final int PROCESSING_MAX_YEAR = 3000;
    public static final int PROCESSING_HDFS_PUT_THREAD_SIZE = 10;
    public static final int PROCESSING_HDFS_PUT_TIMEOUT = 10;
    public static final int PROCESSING_HDFS_PUT_IO_BUFFEER = 4096 * 10;
    public static final int PROCESSING_HDFS_PUT_REPLICATION = 1;

    public static final int PROCESSING_SCHEMAID_SUFFIX_LENGTH = 5;
    public static final String PROCESSING_SCHEMAID_SUFFIX = "00000";
    public static final String PROCESSING_DATA_DID_KEY = "deviceId";
    public static final String PROCESSING_DATA_DTID_KEY = "deviceTypeId";
    public static final String PARTITION_VALUE_SEPERATOR = "##";

    public static final String PROCESSING_FLAGS_DEFAULT_DATABASE = "test_flag";
    public static final String PROCESSING_FLAGS_DEFAULT_BATCH_TABLE = "test_batch";
    public static final String PROCESSING_FLAGS_DEFAULT_STREAM_TABLE = "test_stream";
    public static final String PROCESSING_FLAGS_DEFAULT_K2DB_TABLE = "test_k2db";
    public static final String MYSQL_DRIVER_NAME_DEFAULT = "com.mysql.jdbc.Driver";

    public static final boolean PROCESSING_MR_SKIP_EXCEPTION = true;
    public static final int DATA_TRANSFORM_REDUCE_NUMBER = 30;
    public static final int DATA_TRANSFORM_REDUCE_MIN_NUMBER = 5;
    public static final float DATA_TRANSFORM_BATCH_SHUFFLE_MEMORY_PERCENT = 0.5f;

    //dataplatform storage constants
    public static final String STORAGE_MR_INPUT = "mapreduce.input.fileinputformat.inputdir";
    public static final String STORAGE_MR_ABNORMAL_OUTPUT = "mapred.output.abnormal.dir";
    public static final String STORAGE_MR_OUTPUT = "mapred.output.dir";
    public static final boolean STORAGE_MR_DELETE_INPUT = false;
    public static final boolean STORAGE_MR_DELETE_TMP = true;
    public static final String SCHEMA_FIELD_KEY = "iskey";
    public static final String STORAGE_INPUT_FIELD_SEPARATOR = ",";

    //impala parq load
    public static final int IMPALA_PARQ_PARTITION_TIMEOUT = 300000;
    public static final int IMPALA_PARQ_PARTITION_CHECK_INTERVAL = 300;

    //impala constants
    public static final String IMPALA_DRIVER_CLASS = "org.apache.hive.jdbc.HiveDriver";
    public static final boolean IMPALA_SOFT_DELETE = true;
    public static final int IMPALA_CONN_RETRY_TIME = 5;
    public static final int IMPALA_CONN_GET_TIMEOUT = 60000 * 3; //3min

    public final static String HADOOP_USER_NAME = "hdfs";
    public final static String HADOOP_YARN_USER = "yarn";
    public static final String HADOOP_NAMENODE_HOSTNAME_1 = "namenode1";
    public static final String HADOOP_NAMENODE_HOSTNAME_1_PORT = "8020";
    public static final String HADOOP_NAMENODE_HOSTNAME_2 = "namenode2";
    public static final String HADOOP_NAMENODE_HOSTNAME_2_PORT = "8020";

    //topology parallelism configuration
    public static final int PROTOCOL_TOPOLOGY_WORKER = 1;
    public static final int PROTOCOL_TOPOLOGY_SPOUT_PARALLELISM = 1;
    public static final int PROTOCOL_TOPOLOGY_TRANSFORM_BOLT_PARALLELISM = 1;
    public static final int PROTOCOL_TOPOLOGY_KAFKA_BOLT_PARALLELISM = 1;
    public static final int MONITOR_TOPOLOGY_WORKER = 1;
    public static final int MONITOR_TOPOLOGY_SPOUT_PARALLELISM = 1;
    public static final int MONITOR_TOPOLOGY_BOLT_PARALLELISM = 1;
    public static final int ADAPTER_TOPOLOGY_WORKER = 1;
    public static final int ADAPTER_TOPOLOGY_SPOUT_PARALLELISM = 1;
    public static final int ADAPTER_TOPOLOGY_TRANSFORM_BOLT_PARALLELISM = 1;
    public static final int ADAPTER_TOPOLOGY_KAFKA_BOLT_PARALLELISM = 1;
    public static final int HDFS_TOPOLOGY_WORKER = 1;
    public static final int HDFS_TOPOLOGY_SPOUT_PARALLELISM = 1;
    public static final int HDFS_TOPOLOGY_BOLT_PARALLELISM = 1;
    public static final int AUDIT_TOPOLOGY_WORKER = 1;
    public static final int AUDIT_TOPOLOGY_SPOUT_PARALLELISM = 1;
    public static final int AUDIT_TOPOLOGY_BOLT_PARALLELISM = 1;

    public static final String HDFS_TOPOLOGY_FILE_ROTATIO_SIGN = ".complete";
    public static final String HDFS_TOPOLOGY_DIDEX_ROTATIO_SIGN = ".registered";

    public final static String MONITOR_SPOUT_ID = "dataload-schedule-spout";
    public static final String AUDIT_MAPREDUCE_TABLE = "AUDIT_MAPREDUCE_TABLE";
    public static final String AUDIT_IMPALA_TABLE = "AUDIT_IMPALA_TABLE";

    // audit constants
    public final static boolean AUDIT_TURN_ON = true;
    public final static int AUDIT_INTERVAL_SECONDS = 60;
    public final static String AUDIT_REST_API_ROOTPATH = "/storm";
    public final static String AUDIT_DB_DRIVER = "com.mysql.jdbc.Driver";
    public final static String TOPOLOGY_ENTITY = "topologies";
    public final static String AUDITOR_ENTITY = "auditors";
    public final static String COUNTERS_ENTITY = "counters";
    public final static int AUDIT_SERVER_PORT = 8087;
    public final static String AUDIT_FREQ_CACHE_NAME = "auditFrequentlyCache";
    public final static String AUDIT_RARELY_CACHE_NAME = "auditRarelyCache";
    public final static int AUDIT_RARELY_CACHE_LIVE_SECONDS = 28800;//60*60*8s=8h
    public final static int AUDIT_FREQ_CACHE_LIVE_SECONDS = 300;
    public final static int AUDIT_METRICS_QUERY_MAXSIZES = 10000;//1 year data num = 60mins*24h*365d
    public final static int AUDIT_METRICS_QUERY_SUM_SIZE_TAG = 10002;//JUST A TAG
    public final static int AUDIT_INITIAL_CONNECTIONS_NUMBER = 5;
    public final static int AUDIT_MAX_CONNECTIONS_NUMBER = 10;
    public final static int AUDIT_SAVE_DATA_FREQ_SECS = 20;
    public final static int AUDIT_DATA_CACHE_MAX_SIZE = 10000;
    public final static String AUDIT_AGGREGATE_TABLE_HOUR = "stats_by_hour";
    // netty client constants
    public final static boolean DISTINGUISH_MESSAGES = true;

    //message queue broker url
    public final static String MQ_SERVER_URL = "tcp://192.168.130.29:61616";
    public final static String SCHEMA_QUEUE_SUFFIX = "";

    //message handler thread pool size
    public final static int MESSAGE_HANDLER_THREAD_SIZE = 20;

    // k2 user namesapce
    public final static String K2_USER_NAMESPACE_HEADER = "X-K2Data-API-key";
    public final static String K2_USER_NAMESPACE = "defaultUser";

    //configuration for mock-data
    public final static String REST_DATA_SERVICE_URL = "http://localhost:18080/data-service/v1";

    //sdm rest api constants
    public static final int SDM_REST_API_RETRY_TIMES = 5;
    public static final int SDM_REST_API_SLEEP_TIME = 1000;
    public static final String SDM_REST_API_ASSET_PATH = "/ddm/assets";
    public static final String SDM_REST_API_FIELDGROUP_PATH = "/ddm/field-groups";

    public static final boolean DEBUG_MODE = false;
    public static final boolean DID_AUTO_REGISTER_ENABLE = true;

    public static final String AVRO_TIMESTAMP_NANOS_TYPE = "timestamp-nanos";

    //max schema cache size
    public static final int SCHEMA_CACHE_SIZE = 100;
}
