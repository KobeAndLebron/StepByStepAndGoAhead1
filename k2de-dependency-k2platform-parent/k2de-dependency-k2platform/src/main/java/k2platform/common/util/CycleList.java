package k2platform.common.util;

/**
 * Created by ming on 16-5-4.
 */
public class CycleList {

    public final static String CYCLE_SEPARATOR = ",";

    private long[] cycles;

    public CycleList() {
    }

    public CycleList(long[] cycles) {
        this.cycles = cycles;
    }

    public long[] getCycles() {
        return this.cycles;
    }

    public void setCycles(long[] cycles) {
        this.cycles = cycles;
    }

    public static long[] getCycleListFromStr(String str) {
        if (str == null || str.length() == 0)
            return new long[0];

        String[] cycleStrs = str.split(CYCLE_SEPARATOR);
        long[] cycle = new long[cycleStrs.length];
        for (int i = 0; i < cycle.length; i++) {
            cycle[i] = Long.parseLong(cycleStrs[i]);
        }
        return cycle;
    }
}
