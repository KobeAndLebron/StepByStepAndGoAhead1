package k2platform.common.util;

import org.apache.curator.framework.recipes.cache.TreeCache;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Ming on 2016/4/20.
 */
public abstract class TreeWatcher<T> implements IWatcher {

    private String basePath;
    private Map<String, T> childrenData;
    private TreeCache treeCache;

    public TreeWatcher() {
        this("");
    }

    public TreeWatcher(String basePath) {
        super();
        childrenData = new ConcurrentHashMap<>();
        this.basePath = basePath;
    }

    public void setTreeCache(TreeCache treeCache) {
        this.treeCache = treeCache;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getBasePath() {
        return this.basePath;
    }

    public void closeTreeCache() throws IOException {
        this.treeCache.close();
    }

    public T getChildData(String childName) {
        return childrenData.get(childName);
    }

    public Map<String, T> getChildrenData() {
        return childrenData;
    }

    public void clearChildrenData() {
        childrenData.clear();
    }

    public void clear() {
        clearChildrenData();
    }

    public abstract void load(Map<String, Map> childrenDatas);

    public abstract void update(String childName, byte[] data);

    public abstract void add(String childName, byte[] data);

    public abstract void remove(String childName);
}
