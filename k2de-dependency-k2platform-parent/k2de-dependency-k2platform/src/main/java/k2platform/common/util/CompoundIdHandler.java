package k2platform.common.util;

import java.security.MessageDigest;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CompoundIdHandler {
	public static String map2MD5(LinkedHashMap<String,String> map){  
	    String inStr = map.toString();
        MessageDigest md5 = null;  
        try{  
            md5 = MessageDigest.getInstance("MD5");  
        }catch (Exception e){  
            System.out.println(e.toString());  
            e.printStackTrace();  
        }  
        char[] charArray = inStr.toCharArray();  
        byte[] byteArray = new byte[charArray.length];  
  
        for (int i = 0; i < charArray.length; i++)  
            byteArray[i] = (byte) charArray[i];  
        byte[] md5Bytes = md5.digest(byteArray);  
        StringBuffer hexValue = new StringBuffer();  
        for (int i = 0; i < md5Bytes.length; i++){  
            int val = ((int) md5Bytes[i]) & 0xff;  
            if (val < 16)  
                hexValue.append("0");  
            hexValue.append(Integer.toHexString(val));  
        }  
        return hexValue.toString();  
    }  
	
	public static LinkedHashMap<String,String> sortCompoundId(Map<String,String> compoundId ){
		   TreeMap<String,String> compoundIdTree = new TreeMap<String,String>();
		   Set<String> set = compoundId.keySet();
		   for(String s:set){
			   compoundIdTree.put(s, compoundId.get(s));
		   }
		   set = compoundIdTree.keySet();
		   LinkedHashMap<String,String> sortedCompoundId = new LinkedHashMap<String,String>();
		   for(String s:set){
			   sortedCompoundId.put(s, compoundId.get(s));
		   }
		   return sortedCompoundId;
	   }
}
