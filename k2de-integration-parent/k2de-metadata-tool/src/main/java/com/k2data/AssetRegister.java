package com.k2data;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;


/**
 * Created by lijianing on 17-11-8.
 */
public class AssetRegister {
    public static String c_vib = "{\n" +
            "    \"fieldGroupId\": \"c_vib\",\n" +
            "    \"compoundId\": {\n" +
            "        \"factory_id\": \"%s\",\n" +
            "        \"equipment_id\": \"%s\",\n" +
            "        \"machine_id\": \"%s\",\n" +
            "        \"channel_type\": \"%s\",\n" +
            "        \"channel_id\": \"%s\",\n" +
            "        \"datatype\": \"%s\"\n" +
            "    }\n" +
            "}";

    public static String c_sta = "{\n" +
            "    \"fieldGroupId\": \"c_sta\",\n" +
            "    \"compoundId\": {\n" +
            "        \"factory_id\": \"%s\",\n" +
            "        \"equipment_id\": \"%s\",\n" +
            "        \"machine_id\": \"%s\",\n" +
            "        \"channel_type\": \"%s\",\n" +
            "        \"channel_id\": \"%s\",\n" +
            "        \"datatype\": \"%s\"\n" +
            "    }\n" +
            "}";
    public static void main(String[] args) {
        FileSystem fileSystem = null;
        String postJson = null;
        String sdm = args[0];
        String dir = args[1];
        String hdfsIp = args[2];
        String urlStr = "http://" + sdm + ":8081/data-service/v2/assets";
        try {
            URI uri = URI.create("hdfs://" + hdfsIp);
            Configuration configuration = new Configuration();
            configuration.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
            fileSystem = FileSystem.get(uri, configuration);
            RemoteIterator<LocatedFileStatus> files = fileSystem.listFiles(new Path(dir), true);
            while (files.hasNext()) {
                LocatedFileStatus file = files.next();
                FSDataInputStream fsDataInputStream = fileSystem.open(file.getPath());
                BufferedReader reader = new BufferedReader(new InputStreamReader(fsDataInputStream));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    String[] ids = line.split("\\|");
                    if(ids[0].equals("c_vib")) {
                        postJson = String.format(c_vib, ids[1], ids[2], ids[3], ids[4], ids[5], ids[6]);
                        Request.Post(urlStr).bodyString(postJson, ContentType.APPLICATION_JSON).execute().returnResponse();
                    }
                    if(ids[0].equals("c_sta")) {
                        postJson = String.format(c_sta, ids[1], ids[2], ids[3], ids[4], ids[5], ids[6]);
                        Request.Post(urlStr).bodyString(postJson, ContentType.APPLICATION_JSON).execute().returnResponse();
                    }
                }
                try {
                    fsDataInputStream.close();
                    reader.close();
                } catch (Exception e) {}
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(fileSystem != null)
                    fileSystem.close();
            } catch (Exception e) {}
        }
    }
}
